package de.benediktstuhrmann.drawingtests;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

/**
 * "Zeichenblock" Activity, welche die Levelansicht repräsentiert und dem Nutzer ermöglicht die
 * Aufgabe per Touch zu lösen. Von hier aus wird auch die Überprüfung der Eingabe vorgenommen.
 * @author      Benedikt Stuhrmann
 */
public class DrawGraphActivity extends Activity {

    //Layoutelemente
    Button btn_reset;
    Button btn_confirm;
    TouchEventView tev;
    TextView task;
    TextView taskCount;
    RatingBar strikes;
    private ToggleButton intSwitch;
    private ToggleButton helpSwitch;

    //Gibt an ob die Eingabe bereits bestätigt wurde
    public boolean solution = false;
    //Datenbank
    private UserDataSource uds = new UserDataSource(this);
    //Alle Aufgaben des Levels
    List<Entry> tasks;
    //Aktuelle Aufgabe
    private int taskId = 0;
    private Entry activeTask;
    //Intervallgrenzen der aktuellen Aufgabe
    private int iStart;
    private int iEnd;
    //GridSetting (für Koordinatensystem) der aktuellen Aufgabe
    private float[] gridSettings;

    private int themenId;
    private int levelId;
    private boolean lvlCompleted = true;
    //Liste mit allen Daten über die Aufgaben, welche für die Levelauswertung benötigt werden
    public static List<LevelEvaluation> levelEvaluations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_graph);

        this.levelEvaluations = new ArrayList<>();

        //Das in der Levelauswahl gewählte Level auslesen und anschließend aus der DB lesen
        this.themenId = getIntent().getIntExtra("Themen_ID", -1);
        this.levelId = getIntent().getIntExtra("Level_ID", -1);
        if(this.themenId == -1 || this.levelId == -1)
        {
            System.out.println("Fehler bei ID Übergabe");
            Intent swapToLevels= new Intent(this, LevelSelectActivity.class);
            startActivity(swapToLevels);
        }

        //Aufgaben aus der Datenbank lesen
        try {
            uds.open();
            this.tasks = uds.getAllTasksOfLevel(this.themenId, this.levelId);
            uds.close();
        }
        catch(Exception e) {
            System.out.println("Fehler beim auslesen der Aufgaben des Levels aus der Datenbank");
        }

        // alle Level für Auswertung vorbereiten
        for(int i=0; i<this.tasks.size(); i++) {
            LevelEvaluation lvl = new LevelEvaluation("Aufgabe "+ (i+1));
            lvl.setTask(this.tasks.get(i).getTask());
            lvl.setCompleted(false);
            this.levelEvaluations.add(lvl);
        }

        //Aktuelle Aufgabe auswählen
        this.updateActiveTask();

        //Buttons
        this.initializeButtons();

        //Bereich in dem gemalt werden kann
        tev = (TouchEventView) findViewById(R.id.drawZone);
        tev.setFunctionInfo(this.activeTask.getFunction(), this.iStart, this.iEnd, this.gridSettings);

        //PopUp Fenster mit Aufgabentext anzeigen
        this.showTaskPopUp();

        //Interface initialisieren
        this.initializeInterface();

    }//end onCreate()

    /**
     * Initialisiert den Löschen-, Bestätigen- Intervall-, und Hilfslinien-Button.
     */
    private void initializeButtons()
    {
        btn_reset = (Button) findViewById(R.id.btn_reset);
        btn_confirm = (Button) findViewById(R.id.btn_confirm);

        btn_reset.setVisibility((View.VISIBLE));
        btn_confirm.setVisibility((View.VISIBLE));

        //Löschen-Button
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // der Canvas wir auf Knopfdruck zurückgesetzt
                tev.delete();
            }
        });

        //Bestätigen-Button
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Ausblenden des "Löschen" und des "Bestätigen" Buttons
                btn_reset.setVisibility((View.GONE));
                btn_confirm.setVisibility((View.GONE));
                //Malen unterbinden
                tev.lock(true);
                //Lösung anzeigen
                solution = true;
                //Versuchsanzeige aktualisieren
                updateStrikesBar();
                //Warte 1 Sekunde und zeige dann das Ergebnis an
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Zeige Ergebnis in PopUp Fenster an
                        showGrading();
                    }
                }, 2000);
            }
        });

        //Intervall anzeigen mittels ToggleButton
        intSwitch = (ToggleButton) findViewById(R.id.toggleBtn_interval);
        intSwitch.setChecked(false); //Standardmäßig aus
        intSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Eingeschaltet
                    tev.showIntervallBorders(iStart, iEnd);
                } else {
                    // Ausgeschaltet
                    tev.hideIntervallBorders();
                }
            }
        });

        //Hilfslinien Modus aktivieren
        helpSwitch = (ToggleButton) findViewById(R.id.toggleBtn_guides);
        helpSwitch.setChecked(false); //Standardmäßig aus
        helpSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Eingeschaltet
                    btn_confirm.setVisibility((View.GONE));
                    tev.drawGuidelines(true);
                } else {
                    // Ausgeschaltet
                    btn_confirm.setVisibility((View.VISIBLE));
                    tev.drawGuidelines(false);
                }
            }
        });

        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/pink.ttf");
        btn_reset.setTypeface(typeFace);
        btn_confirm.setTypeface(typeFace);
        helpSwitch.setTypeface(typeFace);
        intSwitch.setTypeface(typeFace);

    }//initializeButtons()

    /**
     * Initialisiert das Interface.
     */
    private void initializeInterface()
    {
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/pink.ttf");

        //Funktion oben im Bildschirm anzeigen
        task = (TextView) findViewById(R.id.task);
        task.setTypeface(typeFace);
        task.setText("f(x) = " + this.activeTask.getTask() + "   [" + this.activeTask.getIntervall_start() + ";" + this.activeTask.getIntervall_end() + "]");

        //Aufgabe X/Y
        taskCount = (TextView) findViewById(R.id.taskXofY);
        taskCount.setTypeface(typeFace);
        taskCount.setText("Aufgabe " + (this.taskId+1) + "/" + this.tasks.size());

        //Strike-/Versuchsanzeige
        strikes = (RatingBar) findViewById(R.id.strikesBar);
        strikes.setRating(0);
    }//end initializeInterface()

    /**
     * Updated die aktuelle Aufgabe, abhängig von der taskId
     */
    private void updateActiveTask()
    {
        //Aktuelle Aufgabe auswählen
        this.activeTask = this.tasks.get(this.taskId);
        this.iStart = activeTask.getIntervall_start();
        this.iEnd = activeTask.getIntervall_end();
        this.gridSettings = new float[] {this.activeTask.getZoom(), this.activeTask.getFunct_start(), this.activeTask.getFunct_end()};
    }//end updateActiveTask()

    /**
     * Öffnet ein PopUp Fenster, welches die Aufgabenstellung der aktuellen Aufgabe anzeigt
     */
    private void showTaskPopUp()
    {
        //PopUp Fenster mit Aufgabentext anzeigen
        Intent swapToSelect = new Intent(this, TaskPopUpActivity.class);
        swapToSelect.putExtra("Task", this.activeTask.getTask());
        swapToSelect.putExtra("Interval_Start", this.activeTask.getIntervall_start());
        swapToSelect.putExtra("Interval_End", this.activeTask.getIntervall_end());
        startActivity(swapToSelect);
    }//end showTaskPopUp()

    /**
     * Aktualisiert die Versuchsanzeige und zieht einen Versuch ab,
     * wenn der Nutzer die Aufgabe nicht lösen konnte
     */
    private void updateStrikesBar()
    {
        //Strike hinzufügen, wenn der Spieler die Aufgabe nicht lösen konnte
        if(this.tev.getGrade() == 4)
            this.strikes.setRating(this.strikes.getRating()+1);
    }//end updateStrikesBar()

    /**
     * Öffnet ein PopUp Fenster, welches die erzielte Bewertung für die jeweilige Aufgabe anzeigt
     */
    private void showGrading()
    {
        //PopUp Fenster mit Bewertung anzeigen
        Intent swapToSelect = new Intent(DrawGraphActivity.this, ResultPopUpActivity.class);
        swapToSelect.putExtra("ScorePoints", this.tev.getScore());
        swapToSelect.putExtra("Grade", this.tev.getGrade());
        int rat = (int) this.strikes.getRating();
        swapToSelect.putExtra("Strikes", rat);

        startActivityForResult(swapToSelect, 0);

        //wenn Spieler noch nicht komplett durch Aufgabe durchgefallen, Buttons wieder einblenden (UND wenn er es erneut versuchen kann)
        if(strikes.getRating() != 3 && tev.getGrade() == 4) {
            findViewById(R.id.btn_reset).setVisibility((View.VISIBLE));
            findViewById(R.id.btn_confirm).setVisibility((View.VISIBLE));
        }
    }//end showGrading()

    /**
     * Wird genutzt, um je nach Bewertung der Aufgabe, NACH dem schließen des Result-PopUps fortzufahren
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //wenn bestanden
        if(data.getIntExtra("nextTask", -1) == 1)
        {
            //zur nächstes Aufgabe
            this.taskId++;
            //wenn noch weitere Aufgaben vorhanden
            if(this.taskId < this.tasks.size())
            {
                //für nächste Aufgabe vorbereiten
                this.putIntoLvlEvalus();
                this.tev.showSolutionPath(false);
                this.tev.delete();
                this.updateActiveTask();
                this.initializeInterface();
                this.initializeButtons();
                this.showTaskPopUp();
                tev.setFunctionInfo(this.activeTask.getFunction(), this.iStart, this.iEnd, this.gridSettings);
                tev.lock(false);
            }//Ende nächste Aufgabe
            //wenn keine Aufgaben mehr vorhanden
            else
            {
                //zur Levelauswertung
                this.putIntoLvlEvalus();
                System.out.println("Alle Aufgaben des Levels abgeschlossen!");

                Intent toEvaluation = new Intent(this, LevelEvaluationActivity.class);
                toEvaluation.putExtra("ThemeId", this.themenId);
                toEvaluation.putExtra("LevelPos", this.levelId);
                toEvaluation.putExtra("Completed", this.lvlCompleted);
                startActivity(toEvaluation);
            }//Ende Levelauswertung
        }
        //wenn erneut versuchen
        else if(data.getIntExtra("nextTask", -1) == 0)
        {
            //erneut versuchen
        }
        //wenn nicht bestanden
        else if(data.getIntExtra("nextTask", -1) == -1){
            //Durchgefallen
            taskId++;
            this.lvlCompleted = false;
            this.putIntoLvlEvalus();
            Intent toEvaluation = new Intent(this, LevelEvaluationActivity.class);
            toEvaluation.putExtra("ThemeId", this.themenId);
            toEvaluation.putExtra("LevelPos", this.levelId);
            toEvaluation.putExtra("Completed", this.lvlCompleted);
            startActivity(toEvaluation);
        }//Ende Durchgefallen
    }//end onActivityResult()

    /**
     * Speichert alle während der Aufgabe gesammelten Informationen, in ein für diese Aufgabe angelegtes LevelEvaluation Objekt.
     */
    private void putIntoLvlEvalus()
    {
        LevelEvaluation lvlEvalu = this.levelEvaluations.get(this.taskId-1);
        lvlEvalu.setScore(this.tev.getScore());
        lvlEvalu.setStrikes((int) this.strikes.getRating());
        if(this.tev.getGrade() < 4)
            lvlEvalu.setCompleted(true);
        lvlEvalu.setScreenshot(this.takeScreenshot());
        this.levelEvaluations.set(this.taskId-1, lvlEvalu);

    }//end putIntoLvlEvalus()

    /**
     * Macht einen Screenshot vom bereich in dem der Spieler seine Kurve malen kann (drawZone)
     * @return      Screenshot als Bitmap
     */
    private Bitmap takeScreenshot()
    {
        View screenshotMe = findViewById(R.id.drawZone);
        screenshotMe.setDrawingCacheEnabled(true);
        Bitmap screenshot = Bitmap.createBitmap(screenshotMe.getDrawingCache());
        screenshotMe.setDrawingCacheEnabled(false);
        return screenshot;
    }//end takeScreenshot()

}//end class