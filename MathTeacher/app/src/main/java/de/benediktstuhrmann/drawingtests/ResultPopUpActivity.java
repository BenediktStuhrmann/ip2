package de.benediktstuhrmann.drawingtests;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.TextView;

public class ResultPopUpActivity extends Activity {

    //Layoutelemente
    private TextView score;
    private TextView grading;

    //errechnete Bewertung für jeweilige Aufgabe
    private int gradeId;
    //Anzahl an gebrauchten Versuche
    private int strikes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_pop_up);

        //Größe des PopUp-Fensters setzen
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width*0.7), (int) (height*0.7));

        //Ergebnis anzeigen
        grading = (TextView) findViewById(R.id.grading);
        score = (TextView) findViewById(R.id.score);
        this.gradeId = getIntent().getIntExtra("Grade", 0);
        this.strikes = getIntent().getIntExtra("Strikes", 0);
        Typeface typeFace = Typeface.createFromAsset(getAssets(),"fonts/pink.ttf");
        grading.setTypeface(typeFace);
        score.setTypeface(typeFace);
        grading.setText(this.gradingMessage(this.gradeId));
        score.setText("Deine Punktzahl: " + getIntent().getIntExtra("ScorePoints", 0));
    }//end onCreate()

    /**
     * Wandelt die übergebene Bewertung in einen entsprechenden Bewertungs-String um
     * @param grade         Bewertung
     * @return              Ein der Bewertung entsprechender String
     */
    private String gradingMessage(int grade)
    {
        switch(gradeId)
        {
            case 1:
                return "Perfekt!";
            case 2:
                return "Gut!";
            case 3:
                return "Das kannst du besser!";
            case 4:
                if(this.strikes < 3)
                    return "Versuchs nochmal!";
                else
                    return "Leider durchgefallen!";
            default:
                return "Hier ist was schiefgelaufen!";
        }
    }//end gradingMessage()

    @Override
    public boolean onTouchEvent(MotionEvent evt)
    {
        switch (evt.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                if(strikes < 3 && this.gradeId < 4) //wenn Aufgabe bestanden
                {
                    //weiter zur nächsten Aufgabe des Levels
                    Intent backToDraw = new Intent();
                    backToDraw.putExtra("nextTask", 1);
                    setResult(RESULT_OK, backToDraw);
                    finish();
                }
                else if(this.gradeId == 4) //wenn Fehlversuch
                {
                    if(strikes >= 3){
                        //wenn keine versuche mehr, => durchgefallen
                        Intent backToDraw = new Intent();
                        backToDraw.putExtra("nextTask", -1);
                        setResult(RESULT_OK, backToDraw);
                        finish();
                    }
                    else {
                        //ansonsten PopUp schließen und erneut versuchen
                        Intent backToDraw = new Intent();
                        backToDraw.putExtra("nextTask", 0);
                        setResult(RESULT_OK, backToDraw);
                        finish();
                    }
                }

                return true;
            default:
                return false;
        }
    }//end onTouchEvent()

}//end ResultPopUpActivity
