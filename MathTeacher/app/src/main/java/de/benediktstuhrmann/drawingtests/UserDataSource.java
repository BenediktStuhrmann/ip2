package de.benediktstuhrmann.drawingtests;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by nnamf on 24.11.2016.
 */

// Klasse mit allen Befehle zum Eintragen und Auslesen in die Datenbak
public class UserDataSource {

    // Datenbank
    private SQLiteDatabase database;
    // MySQLiteHelper
    private MySQLiteHelper dbHelper;
    // String mit allen Spalten von Thema
    private String[] allColumnsTheme = { "THEME_ID", "LABEL", "PROGRESS"};
    // String mit allen Spalten von Level
    private String[] allColumnsLevel = { "NAME", "COMPLETED", "THEME_ID", "LVL_POSITION"};
    // String mit allen Spalten von Aufgaben
    private String[] allColumnsTask = { "THEME_ID", "LVL_POSITION", "TASK_ID", "TASK", "FUNCTION", "INTERVAL_START", "INTERVAL_END", "ZOOM", "FUNCT_START", "FUNCT_END"};

    //dbHelper als neuen SQLiteHelper festlegen
    public UserDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    /** Methode zum öffnen der Datenbank
     *
     * @throws SQLException Exception
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /** Methode zum schließen der Datenbank
     *
     */
    public void close() {
        dbHelper.close();
    }

    //Einfügen der benötigen Daten in die DB
    public void init() throws SQLException {

        try {

            //Themen
            this.createEntryTheme(0, "Geraden",                      0);
            this.createEntryTheme(1, "Parabeln",                     0);
            this.createEntryTheme(2, "Trigonometrische Funktionen",  0);

            //Level
            //Geraden
            this.createEntryLevel("Einsteiger",  0, 0, 0);
            this.createEntryLevel("Novize",      0, 0, 1);
            this.createEntryLevel("Meister",     0, 0, 2);

            //Parabeln
            this.createEntryLevel("Einsteiger",  0, 1, 0);
            this.createEntryLevel("Novize",      0, 1, 1);
            this.createEntryLevel("Meister",     0, 1, 2);

            //Trigos
            this.createEntryLevel("Einsteiger",   0, 2, 0);
            this.createEntryLevel("Novize",       0, 2, 1);
            this.createEntryLevel("Meister",      0, 2, 2);

            //Aufgaben
            //für Geraden Level 1
            this.createEntryTask(0, 0, 0, "0,5x",    "4;0;0;0.5;0;", -4, 1, 100, -6, 6);
            this.createEntryTask(0, 0, 1, "x-1",     "4;0;0;1;-1;",   -1, 4, 100, -6, 4);
            this.createEntryTask(0, 0, 2, "0,5",     "4;0;0;0;0.5;", -3, 3, 100, -6, 6);
            //für Geraden Level 2
            this.createEntryTask(0, 1, 0, "x-2+2",   "4;0;0;1;0;",   -4, 3, 50, -6, 6);
            this.createEntryTask(0, 1, 1, "-0,5x+1", "4;0;0;-0.5;1;", -2, 3, 100, -6, 6);
            this.createEntryTask(0, 1, 2, "2+0,5",   "4;0;0;0;2.5;", -8, 6, 50, -11, 11);
            //für Geraden Level 3
            this.createEntryTask(0, 2, 0, "3+0,4x-3",    "4;0;0;0.4;0;", -4, 3, 100, -6, 6);
            this.createEntryTask(0, 2, 1, "0,6x+1",    "4;0;0;0.6;1;", -5, 3, 50, -8, 8);
            this.createEntryTask(0, 2, 2, "2x+3",    "4;0;0;2;3;", -4, 0, 50, -4, 4);

            //für Parabeln Level 1
            this.createEntryTask(1, 0, 0, "0,5x^2",     "4;0;0,5;0;0;",   -2, 2, 50, -4, 4);
            this.createEntryTask(1, 0, 1, "-0,5x^2 +3", "4;0;-0,5;0;3;", -3, 3, 50, -5, 5);
            this.createEntryTask(1, 0, 2, "-(x+1)^2 +1","4;0;-1;-2;0;",  -3, 2, 75, -3, 1);
            //für Parabeln Level 2
            this.createEntryTask(1, 1, 0, "(x-2)^2 -2", "4;0;1;-4;2;",  0, 4, 100, -1, 5);
            this.createEntryTask(1, 1, 1, "(x+1)^2 -2", "4;0;1;2;-1;",  -3, 1, 100, -4, 2);
            this.createEntryTask(1, 1, 2, "0,2x^2 -2",  "4;0;0,2;0;-2;",  -4, 4, 100, -6, 6);
            //für Parabeln Level 3
            this.createEntryTask(1, 2, 0, "x^2 +2x+1", "4;0;1;2;-1;",   -3, 1, 100, -4, 2);
            this.createEntryTask(1, 2, 1, "-0,5x^2 +2", "4;0;-0.5;0;2;",  -2, 2, 100, -3, 3);
            this.createEntryTask(1, 2, 2, "1+0,5x^2 -3", "4;0;0.5;0;-2;",  -2, 2, 100, -3, 3);

            //für Trigo Level 1
            this.createEntryTask(2, 0, 0, "sin(x)", "1;1;1;0;0;",  0, 4, 100, -6, 6);
            this.createEntryTask(2, 0, 1, "cos(x)", "2;1;1;0;0;",  0, 4, 100, -6, 6);
            //für Trigo Level 2
            this.createEntryTask(2, 1, 0, "sin(2x)",    "1;1;2;0;0;",   0, 4, 100, -6, 6);
            this.createEntryTask(2, 1, 1, "sin(x)+1,5", "1;1;1;0;0.5;", 0, 4, 100, -6, 6);
            this.createEntryTask(2, 1, 2, "2cos(2x)",   "2;2;2;0;0;",  -2, 2, 100, -6, 6);
            //für Trigo Level 3
            this.createEntryTask(2, 2, 0, "sin(x-(pi/2))",  "1;1;1;1,6;0;",   0, 4, 100, -6, 6);
            this.createEntryTask(2, 2, 1, "cos(2x)-0,5",    "1;1;2;0;-0.5;", -3, 1, 100, -6, 6);
            this.createEntryTask(2, 2, 2, "sin(x)-sin(x)+1,5", "4;0;0;0;1,5;",    -3, 2, 100, -6, 6);

            System.out.println("Gepeichert");

        }catch(Exception e)
        {
            System.out.println("Erstell Error");
        }

    }

    //Methode zum löschen und wiedererstellen der DB
    public void deleteAndRecreate()
    {

        try {
            dbHelper.deleteAllTables(database);
            dbHelper.onCreate(database);
            System.out.println("Gelöscht");

        }catch(Exception e)
        {
            System.out.println("Löschen Error");
        }

    }

    //Methode zum Anzeigen des Inhalts der DB
    public void show() {

        try {
            List<Entry> eT = this.getAllEntriesTheme();
            List<Entry> eL = this.getAllEntriesLevel();
            List<Entry> eA = this.getAllEntriesTask();

            System.out.println("--- Themen --------------------------");
            for(Entry e: eT)
                System.out.println(e.getTheme_id() + "  |  " +  e.getLabel() + "  |  " + e.getProgress());

            System.out.println("--- Level ---------------------------");
            for(Entry e: eL)
                System.out.println(e.getName() + "  |  " + e.getCompleted() + "  |  " + e.getTheme_id() + "  |  " +  e.getLvl_position());

            System.out.println("--- Aufgaben-------------------------");
            for(Entry e: eA)
                System.out.println(e.getTheme_id() + "  |  " + e.getLvl_position() + "  |  " + e.getTask_id() + "  |  " +  e.getTask() + "  |  " + e.getFunction() + "  |  " +  e.getIntervall_start() + "  |  " +  e.getIntervall_end() + "  |  " + e.getZoom() + "  |  " + e.getFunct_start() + "  |  " + e.getFunct_end());

            System.out.println(getLevelCount(1));
        }catch(Exception e)
        {
            System.out.println("Auslese Error");
        }

    }



    /** Methode die die Klasse Entry zum speichern eines Eintrages in Thema benutzt
     *
     * @param theme_id
     * @param label
     * @param progress
     * @return
     */
    public /*Entry */ void createEntryTheme(int theme_id, String label, int progress) {
        // Values zum Speichern aller Werte einer Zeile / Eintrag
        ContentValues values = new ContentValues();
        // Spalten die angegebenen Werte zuweisen
        values.put("THEME_ID", theme_id);
        values.put("LABEL", label);
        values.put("PROGRESS", progress);

        // Eintragen der Werte in Thema
        long insertThemeId = database.insert("THEME", null,
                values);

        /*
        // Erstellen eines Cursor
        Cursor cursor = database.query("THEME", allColumnsTheme, "THEME_ID = "+insertThemeId, null, null, null, null);
        cursor.moveToFirst(); // Cursor an den Anfang setzen

        return cursorToEntryTheme(cursor); // Methode zum Abgehen des Entries (unten)
        */
    }


    /** Methode die die Klasse Entry zum speichern eines Eintrages in Level benutzt
     *
     * @param name
     * @param completed
     * @param themen_id
     * @param lvl_position
     * @return
     */
    public /*Entry */ void createEntryLevel(String name, int completed, int themen_id, int lvl_position) {
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("COMPLETED", completed);
        values.put("THEME_ID", themen_id);
        values.put("LVL_POSITION", lvl_position);

        long insertLevelId = database.insert("LEVEL", null, values);

        /*
        Cursor cursor = database.query("LEVEL",allColumnsLevel, "LEVEL_ID = " + insertLevelId, null, null, null, null);
        cursor.moveToFirst();

        return cursorToEntryLevel(cursor);*/
    }


    /** Methode die die Klasse Entry zum speichern eines Eintrages in Aufgabe benutzt
     *
     * @param theme_id
     * @param lvl_position
     * @param task_id
     * @param task
     * @param function
     * @param interval_start
     * @param interval_end
     * @param zoom
     * @param funct_start
     * @param funct_end
     * @return
     */
    public /*Entry */ void createEntryTask(int theme_id, int lvl_position, int task_id, String task, String function, int interval_start, int interval_end, int zoom, int funct_start, int funct_end) {
        ContentValues values = new ContentValues();
        values.put("THEME_ID", theme_id);
        values.put("LVL_POSITION", lvl_position);
        values.put("TASK_ID", task_id);
        values.put("TASK", task);
        values.put("FUNCTION", function);
        values.put("INTERVAL_START", interval_start);
        values.put("INTERVAL_END", interval_end);
        values.put("ZOOM", zoom);
        values.put("FUNCT_START", funct_start);
        values.put("FUNCT_END", funct_end);

        long insertLevelId = database.insert("TASK", null, values);

    }


    /**
     * Methode zum auslesen der Entries von Thema
     * @return Entries als Liste
     */
    protected List<Entry> getAllEntriesTheme() {
        List<Entry> EntriesList = new ArrayList<Entry>();
        EntriesList = new ArrayList<Entry>();

        Cursor cursor = database.query("THEME", allColumnsTheme, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return EntriesList;


        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryTheme(cursor);
            EntriesList.add(entry);
            cursor.moveToNext();
        }

        cursor.close();

        return EntriesList;
    }

    /**
     * Methode zum auslesen der Entries von Level
     * @return Entries als Liste
     */
    protected List<Entry> getAllEntriesLevel() {
        List<Entry> EntriesList = new ArrayList<Entry>();
        EntriesList = new ArrayList<Entry>();

        Cursor cursor = database.query("LEVEL", allColumnsLevel, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return EntriesList;


        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryLevel(cursor);
            EntriesList.add(entry);
            cursor.moveToNext();
        }

        cursor.close();

        return EntriesList;
    }

    /**
     * Methode zum auslesen der Entries von Aufgabe
     * @return Entries als Liste
     */
    protected List<Entry> getAllEntriesTask() {
        List<Entry> EntriesList = new ArrayList<Entry>();
        EntriesList = new ArrayList<Entry>();

        Cursor cursor = database.query("TASK", allColumnsTask, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return EntriesList;


        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryTask(cursor);
            EntriesList.add(entry);
            cursor.moveToNext();
        }

        cursor.close();

        return EntriesList;
    }

    /** Eintrag erstellen für Thema
     *
     * @param cursor Cursor
     * @return entry
     */
    private Entry cursorToEntryTheme(Cursor cursor) {
        //neuer Eintrag
        Entry entry = new Entry();
        entry.setTheme_id(cursor.getLong(0));             //Position in der Tabelle
        entry.setLabel(cursor.getString(1));
        entry.setProgress(cursor.getInt(2));

        return entry;
    }

    /** Eintrag erstellen für Level
     *
     * @param cursor Cursor
     * @return entry
     */
    private Entry cursorToEntryLevel(Cursor cursor) {
        Entry entry = new Entry();
        entry.setName(cursor.getString(0));
        entry.setCompleted(cursor.getInt(1) > 0);
        entry.setTheme_id(cursor.getInt(2));
        entry.setLvl_position(cursor.getInt(3));

        return entry;
    }

    /** Eintrag erstellen für Aufgabe
     *
     * @param cursor Cursor
     * @return entry
     */
    private Entry cursorToEntryTask(Cursor cursor) {
        Entry entry = new Entry();
        entry.setTheme_id(cursor.getLong(0));
        entry.setLvl_position(cursor.getInt(1));
        entry.setTask_id(cursor.getLong(2));
        entry.setTask(cursor.getString(3));
        entry.setFunction(cursor.getString(4));
        entry.setIntervall_start(cursor.getInt(5));
        entry.setIntervall_end(cursor.getInt(6));
        entry.setZoom(cursor.getInt(7));
        entry.setFunct_start(cursor.getInt(8));
        entry.setFunct_end(cursor.getInt(9));

        return entry;
    }

    /**
     * Gibt alle Aufgaben in einer Entry-List für das spezifizierte Level zurück.
     * @param theme_id      Level
     * @param lvl_position
     * @return              Alle Aufgaben des Levels
     */
    public List<Entry> getAllTasksOfLevel(int theme_id, int lvl_position)
    {
        List<Entry> EntriesList = new ArrayList<Entry>();
        EntriesList = new ArrayList<Entry>();

        Cursor cursor = database.query("TASK", allColumnsTask, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return EntriesList;


        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryTask(cursor);
            if(entry.getTheme_id() == theme_id && entry.getLvl_position() == lvl_position) {
                EntriesList.add(entry);
            }
            cursor.moveToNext();
        }

        cursor.close();

        return EntriesList;
    }//end getAllTasksOfLevel

    /** Gibt zurück ob ein Level abgeschlossen wurde oder nicht.
     * Neue Methode (Max)
     * @param theme_id
     * @param lvl_postion
     * @return
     */
    public boolean getCompletedOfLevel(int theme_id, int lvl_postion)
    {

        boolean completed = false;
        List<Entry> EntriesList = new ArrayList<>();

        Cursor cursor = database.query("LEVEL", allColumnsLevel, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return completed;

        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryLevel(cursor);
            if(entry.getTheme_id() == theme_id && entry.getLvl_position() == lvl_postion) {
                EntriesList.add(entry);
                completed = entry.getCompleted();
            }
            cursor.moveToNext();
        }

        cursor.close();

        return completed;


    }//end getCompletedofLevel

    /** Gibt den Namen eines levels zurück.
     *
     * @param theme_id
     * @param lvl_postion
     * @return Name eines Levels
     */
    public String getNameOfLevel(int theme_id, int lvl_postion)
    {

        String name = "";
        List<Entry> EntriesList = new ArrayList<>();

        Cursor cursor = database.query("LEVEL", allColumnsLevel, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return "DOES NOT EXIST";

        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryLevel(cursor);
            if(entry.getTheme_id() == theme_id && entry.getLvl_position() == lvl_postion) {
                EntriesList.add(entry);
                name = entry.getName();
            }
            cursor.moveToNext();
        }

        cursor.close();

        return name;


    }//end getNameOfLevel

    /** Gibt den Namen eines Themas.
     * Neue Methode (Max)
     * @param theme_id
     * @return
     */
    public String getNameOfTheme(int theme_id)
    {

        String name = "";
        List<Entry> EntriesList = new ArrayList<Entry>();
        EntriesList = new ArrayList<Entry>();

        Cursor cursor = database.query("THEME", allColumnsTheme, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return "DOES NOT EXIST";


        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryTheme(cursor);
            if(entry.getTheme_id() == theme_id) {
                EntriesList.add(entry);
                name = entry.getLabel();
            }
            cursor.moveToNext();
        }

        cursor.close();

        return name;

    }//end getNameOfTheme


    /** Gibt den Progress eines Themas zurück
     *
     * @param theme_id
     * @return progress
     */
    public int getProgressOfTheme(int theme_id)
    {

        int progress = -1;
        List<Entry> EntriesList = new ArrayList<Entry>();
        EntriesList = new ArrayList<Entry>();

        Cursor cursor = database.query("THEME", allColumnsTheme, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return progress;


        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryTheme(cursor);
            if(entry.getTheme_id() == theme_id) {
                EntriesList.add(entry);
                progress = entry.getProgress();
            }
            cursor.moveToNext();
        }

        cursor.close();

        return progress;

    }//end getProgressOfTheme

    /** Gibt die Anzahl der Level eines Themas zurück
     *
     * @param theme_id
     * @return count
     */
    public int getLevelCount(int theme_id)
    {

        int count = 0;

        Cursor cursor = database.query("LEVEL", allColumnsLevel, null, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() == 0) return count;


        while (cursor.isAfterLast() == false) {
            Entry entry = cursorToEntryLevel(cursor);
            if(entry.getTheme_id() == theme_id)
                count += 1;
            cursor.moveToNext();
        }

        cursor.close();

        return count;


    } //end getLevelCount

    /** Updatet den Fortschritt eines Themas
     *
     * @param theme_id
     * @param progress
     */
    public void updateProgressOfTheme (int theme_id, int progress) {
        String strSQL = "UPDATE THEME SET PROGRESS = " + progress + " WHERE THEME_ID = " + theme_id;
        database.execSQL(strSQL);
    }

    /** Updatet Completed von einem Level
     *
     * @param theme_id
     * @param lvl_position
     * @param completed
     */
    public void updateCompletedOfLevel (int theme_id, int lvl_position, int completed) {
        String strSQL = "UPDATE LEVEL SET COMPLETED = " + completed + " WHERE THEME_ID = " + theme_id + " AND LVL_POSITION = " + lvl_position;
        database.execSQL(strSQL);
    }

}
