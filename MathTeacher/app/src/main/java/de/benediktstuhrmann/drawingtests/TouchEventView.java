package de.benediktstuhrmann.drawingtests;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.Stack;

/**
 * TouchEventView, welcher dem Nutzer ermöglicht eine Kurve
 * per Touch Eingabe zu malen
 *
 * @author Benedikt Stuhrmann
 */
public class TouchEventView extends View
{
    private Context context;
    //Farbe, in welcher der gemalte Pfad des Spielers dargestellt wird
    private Paint paint = new Paint();
    //Pfad, welchen der Spieler per Touch-Eingabe erstellt
    private Path path = new Path();
    //gibt an, den TouchEventView zurückzusetzen
    private boolean reset = false;
    //gibt an, ob der TouchEventView gesperrt ist (kein Malen möglich)
    private boolean locked = false;
    //Pfad, welcher den Lösungsgraphen repräsentiert
    private Path solutionPath;
    //Farbe, in welcher der Lösungsgraph dargestellt wird
    private Paint solutionPaint = new Paint();
    //Farbe, in welcher der tolerierte Abweichungsbereich vom Lösungsgraph dargestellt wird
    private Paint solutionPaint2 = new Paint();
    //Farbe, in welcher Checkpoints dargestellt werden
    private Paint checkpointPaint = new Paint();
    //entscheidet darüber, ob die Lösung angezeigt werden soll
    private boolean drawSolution = false;
    //zeigt Intervallgrenzen visuell an / blendet sie aus
    private boolean showIntervalls = false;
    //Farbe, in welcher Intervallgrenzen dargestellt werden
    private Paint intervallPaint = new Paint();
    //Farbe, in welcher die Koordinatenachsen dargestellt werden
    private Paint coordinateSystemPaint = new Paint();
    //Farbe, in welcher die Beschriftung des Koordinatensystems dargestellt wird
    private Paint coordinateSystemTextPaint = new Paint();
    //Farbe für den Hintergrund des Koordinatensystems
    private Paint coordinateSystemBgPaint = new Paint();

    //aktiviert/deaktiviert Hilfslinienmodus
    private boolean drawGuidesMode = false;
    //gibt an, ob gerade eine Hilflinie gesetzt wird
    private boolean drawingGuide = false;
    //Ausrichtung der aktuellen Hilfslinie
    private boolean newGuideHor = false;
    //Farbe, in welcher Hilflinien angezeigt werden
    private Paint guideLinesPaint = new Paint();
    //Farbe, in welcher die aktuelle Hilflinie angezeigt wird, welche erstellt wird
    private Paint newGuideLinePaint = new Paint();
    //Hilflinie
    private float[] newGuide = new float[4];            // float[] = {xStart, yStart, xEnd, yEnd}
    //alle Hilflinien
    private Stack<float[]> guideLines = new Stack<>();  // float[] = {xStart, yStart, xEnd, yEnd}
    //Position des TouchEvents
    private float[] touchEvtLoc;

    //Checkpoints
    private float[][] checkpoints;
    //Skalierungsfaktor
    private int zoom = 100;
    //Intervallstart und -ende für Intervallanzeige (Screenkoordinaten)
    private float iStart;
    private float iEnd;
    //Intervallstart und -ende für Funktion (Funktionswerte)
    private float iStartF;
    private float iEndF;

    //Punktzahl
    private int score;
    //Bewertung
    private int grade;
    //Funktionsterm
    private String funct;
    //Koordinatensystemeinstellungen
    private float[] gridS;

    /**
     * Konstruktor des TEV (TouchEventViews)
     * @param context       Kontext
     * @param attrSet       AttributSet
     */
    public TouchEventView(Context context, AttributeSet attrSet)
    {
        super(context, attrSet);
        this.context = context;

        //Farbeinstellungen für Usereingabe
        paint.setAntiAlias(true);
        paint.setColor(Color.GREEN);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(11);

        ////Farbeinstellungen für Idealkurve
        solutionPaint.setAntiAlias(true);
        solutionPaint.setColor(Color.rgb(0,0,255));
        solutionPaint.setStrokeJoin(Paint.Join.ROUND);
        solutionPaint.setStyle(Paint.Style.STROKE);
        solutionPaint.setStrokeCap(Paint.Cap.ROUND);
        solutionPaint.setStrokeWidth(11);

        ////Farbeinstellungen für Abweichung
        solutionPaint2.setAntiAlias(true);
        solutionPaint2.setColor(Color.argb(255,255,255,0));
        solutionPaint2.setStrokeJoin(Paint.Join.ROUND);
        solutionPaint2.setStyle(Paint.Style.STROKE);
        solutionPaint2.setStrokeCap(Paint.Cap.ROUND);
        solutionPaint2.setStrokeWidth(20);

        ////Farbeinstellungen für Checkpoints
        checkpointPaint.setAntiAlias(true);
        checkpointPaint.setColor(Color.rgb(255,0,0));

        ////Farbeinstellungen für Intervallgrenzen
        intervallPaint.setColor(Color.argb(100,255,0,0));

        ////Farbeinstellungen für Koordinatensystem
        coordinateSystemPaint.setColor(Color.argb(255,0,0,0));
        coordinateSystemPaint.setStyle(Paint.Style.STROKE);
        coordinateSystemPaint.setStrokeWidth(2);
        coordinateSystemTextPaint.setColor(Color.argb(255,0,0,0));
        coordinateSystemBgPaint.setColor(Color.argb(255,145,227,243));
        coordinateSystemBgPaint.setStyle(Paint.Style.STROKE);
        coordinateSystemBgPaint.setStrokeWidth(2);

        ////Farbeinstellungen für Hilfslinien
        guideLinesPaint.setColor(Color.argb(255,255,0,0));
        newGuideLinePaint.setColor(Color.argb(255,0,0,255));
    }//end TouchEventView()

    @Override
    protected void onDraw(Canvas canvas)
    {
        //Koordinatensystem malen
        drawCoordinateSystem(canvas);

        //Lösungsgraphen malen
        if(this.drawSolution)
        {
            canvas.drawPath(solutionPath, solutionPaint2);
            canvas.drawPath(solutionPath, solutionPaint);
        }

        //UserPfad malen
        canvas.drawPath(path, paint);

        //Checkpoints malen
        if(this.drawSolution) {
            for (int i = 0; i < checkpoints.length; i++)
                canvas.drawCircle(checkpoints[i][0], checkpoints[i][1], 4, checkpointPaint);
        }

        //Hilfslinien malen
        for(float[] guide: this.guideLines)
            canvas.drawLine(guide[0], guide[1], guide[2], guide[3], guideLinesPaint);
        if(this.drawingGuide) {
            canvas.drawLine(newGuide[0], newGuide[1], newGuide[2], newGuide[3], newGuideLinePaint);
            if(this.newGuideHor)
                canvas.drawText("Y: " + (Math.round((-(((newGuide[1])-(this.getHeight()/2))/zoom))*100)/100.0f), touchEvtLoc[0]-80, touchEvtLoc[1], coordinateSystemTextPaint);
            else
                canvas.drawText("X: " + (Math.round(((((newGuide[0])-(this.getWidth()/2))/zoom))*100)/100.0f), touchEvtLoc[0]-80, touchEvtLoc[1], coordinateSystemTextPaint);
        }

        //Intervallgrenzeen malen
        if(this.showIntervalls) {
            canvas.drawRect(0   , 0, iStart         , this.getHeight(), intervallPaint); //Intervallbegrenzung links
            canvas.drawRect(iEnd, 0, this.getWidth(), this.getHeight(), intervallPaint); //Intervallbegrenzung rechts
        }
    }//end onDraw()

    @Override
    public boolean onTouchEvent(MotionEvent evt)
    {
        if(locked)
            return false;

        float x = evt.getX();
        float y = evt.getY();

        //Position des TouchEvents abspeichern
        this.touchEvtLoc = new float[] {x, y};

        if(!drawGuidesMode)
        {
            //Hilfslinien-Mal-Modus AUS
            switch (evt.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (this.reset)
                        this.delete(); //bei Neuansetzten wird der Pfad zuerst zurückgesetzt
                    path.moveTo(x, y); //bei Fingerdruck auf Fingerposition setzen
                    return true;
                case MotionEvent.ACTION_MOVE:
                    path.lineTo(x, y); //Beim Bewegen des Fingers Linie zur FingerPosi zeichnen
                    break;
                case MotionEvent.ACTION_UP:
                    this.reset = true; //Beim Bewegen des Fingers Linie zur FingerPosi zeichnen
                    break;
                default:
                    return false;
            }
        }
        else
        {
            //Hilfslinien-Mal-Modus AN
            switch (evt.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    if(!this.drawingGuide)
                    {
                        //ansonsten Hilfslinie an Fingerposition setzen
                        this.newGuide = new float[4];
                        this.newGuide[0] = 0;
                        this.newGuide[2] = this.getWidth();
                        this.newGuide[1] = y;
                        this.newGuide[3] = y;
                        this.drawingGuide = true;
                        this.newGuideHor = true;
                    }
                    return true;
                case MotionEvent.ACTION_POINTER_DOWN:
                    //wenn bereits eine Hilfslinie gemalt wird, Orientierung ändern
                    if(this.drawingGuide)
                        this.newGuideHor = !this.newGuideHor;
                    this.updateNewGuideLine(x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    this.updateNewGuideLine(x, y);
                    break;
                case MotionEvent.ACTION_UP:
                    this.guideLines.push(newGuide);
                    this.newGuide = new float[4];
                    this.drawingGuide = false;
                    break;
                default:
                    return false;
            }
        }

        invalidate();
        return true;
    }//end onTouchEvent()

    /**
     * Setzt alles Gemalte auf dem Canvas zurück oder
     * löscht die zuletzt gesetzte Hilflinie, sofern der
     * Hilflinienmodus aktiviert ist
     */
    public void delete()
    {
        if(!this.drawGuidesMode)
        {
            this.reset = false;
            path.reset();
        }
        else
            //Die zuletzt gesetzte Hilflinie löschen
            this.guideLines.pop();

        invalidate();
    }//end reset()

    /**
     * Löscht alles auf dem Canvas
     */
    public void reset()
    {
        this.delete();
    }

    /**
     * Unterbindet das Malen auf dem Canvas und initialisiert die Überprüfung des Graphens
     */
    public void lock(boolean lock)
    {
        if(lock) {
            this.locked = true;
            PathChecker pC = new PathChecker(this.context, this, this.path, this.funct, this.gridS);
            pC.check(this.iStartF, iEndF);
        }else
            this.locked = false;
    }//end lock()

    /**
     * Zeichnet die Funktion (lösung)
     * @param p     Funktion (Lösung)
     */
    public void setSolutionPath(Path p)
    {
        this.solutionPath = p;
    }//end setSolutionPath()

    /**
     * Ermöglicht das Anzeigen/Ausblenden des Lösungsgraphen
     * @param show      true: Lösungsgraph wird angezeigt
     *                  false: Lösungsgraph wird nicht angezeigt
     */
    public void showSolutionPath(boolean show)
    {
        if(show)
            this.drawSolution = true;
        else
            this.drawSolution = false;
        invalidate();
    } //end showSolutionPath()

    /**
     * Übergebene Checkpoints werden dargestellt
     * @param points        Checkpoints
     */
    public void drawCheckpoints(float[][] points)
    {
        this.checkpoints = points;
    }//end drawCheckpoints()

    /**
     * Zeigt die Intervallgrenzen visuell an
     * @param iStart    Intervalluntergrenze
     * @param iEnd      Intervallobergrenze
     */
    public void showIntervallBorders(float iStart, float iEnd)
    {
        this.iStart = (iStart*zoom)+(this.getWidth()/2);
        this.iEnd = (iEnd*zoom)+(this.getWidth()/2);
        this.showIntervalls = true;
        invalidate();
    }//end showIntervallBorders()

    /**
     * Blendet die Intervallgrenzen aus
     */
    public void hideIntervallBorders()
    {
        this.showIntervalls = false;
        invalidate();
    }//end hideIntervallBorders()

    /**
     * Malt das Koordinatensystem auf dem Canvas Objekt
     * @param canvas    Canvas Objekt
     */
    private void drawCoordinateSystem(Canvas canvas)
    {
        float xMid = this.getWidth()/2;
        float yMid = this.getHeight()/2;

        //Hintergrund Vertikal
        for(float x = xMid; x<this.getWidth(); x+=25)
            canvas.drawLine(x, 0 , x, this.getHeight(), coordinateSystemBgPaint);
        for(float x = xMid; x>0; x-=25)
            canvas.drawLine(x, 0 , x, this.getHeight(), coordinateSystemBgPaint);
        //Hintergrund Horizontal
        for(float y = yMid; y<this.getHeight(); y+=25)
            canvas.drawLine(0, y , this.getWidth(), y, coordinateSystemBgPaint);
        for(float y = yMid; y>0; y-=25)
            canvas.drawLine(0, y , this.getWidth(), y, coordinateSystemBgPaint);

        float h = 5; //Höhe der Zahlenstriche
        int i = 0;
        //Positiv x-Richtung
        for(float x = xMid+zoom; x<this.getWidth(); x+=zoom) {
            //canvas.drawLine(x-(zoom/2), yMid + (this.getHeight()/2), x-(zoom/2), yMid - (this.getHeight()/2), coordinateSystemBgPaint); //Vertikale an Halber
            //canvas.drawLine(x, yMid + (this.getHeight()/2), x, yMid - (this.getHeight()/2), coordinateSystemBgPaint); //Vertikale an Ganzer

            canvas.drawLine(x, yMid + h, x, yMid - h, coordinateSystemPaint);
            i++;
            canvas.drawText(""+i, x-3, yMid-10, coordinateSystemTextPaint);
        }

        i = 0;
        //Negative x-Richtung
        for(float x = xMid-zoom; x>0; x-=zoom) {
            //canvas.drawLine(x+(zoom/2), yMid + (this.getHeight()/2), x+(zoom/2), yMid - (this.getHeight()/2), coordinateSystemBgPaint); //Vertikale an Halber
            //canvas.drawLine(x, yMid + (this.getHeight()/2), x, yMid - (this.getHeight()/2), coordinateSystemBgPaint); //Vertikale an Ganzer

            canvas.drawLine(x, yMid + h, x, yMid - h, coordinateSystemPaint);
            i--;
            canvas.drawText(""+i, x-3, yMid-10, coordinateSystemTextPaint);
        }

        i=0;
        //Positive y-Richtung
        for(float y = yMid-zoom; y>0; y-=zoom) {
            //canvas.drawLine(xMid + (this.getWidth()/2), y+(zoom/2), xMid - (this.getWidth()/2), y+(zoom/2), coordinateSystemBgPaint); //Horizontale an Halber
            //canvas.drawLine(xMid + (this.getWidth()/2), y, xMid - (this.getWidth()/2), y, coordinateSystemBgPaint); //Horizontale an Ganzer

            canvas.drawLine(xMid + h, y, xMid - h, y, coordinateSystemPaint);
            i++;
            canvas.drawText(""+i, xMid+10, y+3.5f, coordinateSystemTextPaint);
        }

        i=0;
        //Negative y-Richtung
        for(float y = yMid+zoom; y<this.getHeight(); y+=zoom){
            //canvas.drawLine(xMid + (this.getWidth()/2), y-(zoom/2), xMid - (this.getWidth()/2), y-(zoom/2), coordinateSystemBgPaint); //Horizontale an Halber
            //canvas.drawLine(xMid + (this.getWidth()/2), y, xMid - (this.getWidth()/2), y, coordinateSystemBgPaint); //Horizontale an Ganzer

            canvas.drawLine(xMid+h, y, xMid-h, y, coordinateSystemPaint);
            i--;
            canvas.drawText(""+i, xMid+10, y+3.5f, coordinateSystemTextPaint);
        }

        //x-Achse
        canvas.drawLine(0, yMid, this.getWidth(), yMid, coordinateSystemPaint);
        //y-Achse
        canvas.drawLine(xMid, 0, xMid, this.getHeight(), coordinateSystemPaint);

    }// end drawCoordinateSystem()

    public void setFunctionInfo(String funct, float iStart, float iEnd, float[] gridS)
    {
        this.gridS = new float[] {gridS[0], gridS[1], gridS[2]};
        this.funct = funct;
        this.guideLines.clear();
        this.iStartF = iStart;
        this.iEndF = iEnd;
        this.zoom = (int) gridS[0];
    }//end setFunctionInfo()

    public void grading(int score, int grade)
    {
        this.score = score;
        this.grade = grade;
    }//end grading()

    public int getScore()
    {
        return this.score;
    }

    public int getGrade()
    {
        return this.grade;
    }

    public void zoom(int zoom)
    {
        this.zoom = zoom;
    }

    public void drawGuidelines(boolean drawMode)
    {
        this.drawGuidesMode = drawMode;
    }//end drawGuidelines()

    /**
     * Aktualisiert die Position der Hilflinie, welche noch nicht fertig gesetzt ist
     * @param x
     * @param y
     */
    private void updateNewGuideLine(float x, float y)
    {
        if(this.drawingGuide && this.newGuideHor)
        {
            this.newGuide[0] = 0;
            this.newGuide[2] = this.getWidth();
            this.newGuide[1] = y;
            this.newGuide[3] = y;
        }
        else if((this.drawingGuide && !this.newGuideHor))
        {
            this.newGuide[0] = x;
            this.newGuide[2] = x;
            this.newGuide[1] = 0;
            this.newGuide[3] = this.getHeight();
        }
    }//end updateNewGuideLine()

}//end TouchEventView
