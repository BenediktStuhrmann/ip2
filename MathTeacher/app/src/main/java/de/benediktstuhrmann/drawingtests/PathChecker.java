package de.benediktstuhrmann.drawingtests;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.provider.Settings;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Der PathChecker ist in der Lage den vom Nutzer gemalten Graphen zu analysieren
 * und diesen mit der "Lösung" für das jeweilige Level abzugleichen.
 * Aus dieser Auswertung errechnet der PathChecker dann eine Bewertung, welche über den erfolgreichen Abschluss entscheidet.
 *
 * @author Benedikt Stuhrmann
 */
public class PathChecker {

    //Referenz zum Touch Event View über welchen gemalt wird
    private TouchEventView tev;
    //PathMeasure zur Berechnung von Punkten auf dem Pfad
    private PathMeasure pathMeasureUser;
    private PathMeasure pathMeasureSolution;
    //Länge des Pfades
    private float pathLengthUser;
    private float pathLengthSolution;
    //Anzahl der zu machenden Schritte (Checkpoint Anzahl)
    private int stepsUser;
    private int stepsSolution;
    //Abstand von Checkpoint zu Checkpoint
    private float intervalLengthUser;
    private float intervalLengthSolution;
    //Beeinflusst die Anzahl an Checkpoints (Pfadlänge/stepFactor)
    private int stepFactor = 10;
    //EINE Koordinate auf dem vom Nutzer gemalten Graphen {x,y}
    private float[] pCoords = new float[2];
    //x,y Koordinaten ALLER berechneten Punkte (Checkpoints) auf dem Graphen
    private float[][] coordsUser;
    private float[][] coordsSolution;
    //Lösungspfad
    private Path solutionPath;
    //Vergrößerung der Funktion
    private int zoom = 100;
    //erreichte Punktzahl
    private int score = 0;
    //Funktion
    private String funct;
    private float[] gridSettings;
    //kritische Punkte der Funktion
    private HashMap<String, ArrayList<float[]>> critPoints;

    //Punkteverteilung
    private final int MAX_SCORE = 1000;
    private final int MAX_SCORE_FOR_LENGTH = 100;
    private final int MAX_SCORE_FOR_DIFF = 650;
    private final int MAX_SCORE_FOR_CRITS = 250;

    /**
     * Konstruktor für neuen PathChecker
     * @param context   Kontext
     * @param tev       TouchEventView, auf welchem die Eingabe getätigt wurde
     * @param path      Das Pfadobjekt, welches vom Spieler gemalt wurde
     * @param funct     Die Funktion, welche der Spieler malen sollte
     * @param gridS     Einstellungen des Koordinatensystems
     */
    public PathChecker(Context context, TouchEventView tev, Path path, String funct, float[] gridS)
    {
        this.tev = tev;
        this.funct = funct;
        this.gridSettings = gridS;
        this.zoom = (int)gridSettings[0];
        tev.zoom(this.zoom);

        //Userdaten
        this.pathMeasureUser = new PathMeasure(path, false);
        this.pathLengthUser = pathMeasureUser.getLength();
        this.stepsUser = (int) this.pathLengthUser/stepFactor;
        this.intervalLengthUser = pathLengthUser/stepsUser;
        this.coordsUser = new float[stepsUser][2];

        //System.out.println("(User) Pfadlänge      : "+pathLengthUser);
        //System.out.println("(User) Anzahl Schritte: "+stepsUser);
        //System.out.println("(User) Schrittlänge   : "+intervalLengthUser);
    }//end PathChecker()

    /**
     *  Vergleicht den vom Nutzer gemalten Graphen mit der Lösung
     *  und zieht aus dem Vergleich eine Bewertung.
     */
    public void check(float iStart, float iEnd)
    {
        //Checkpoints auf dem Nutzergraphen berechnen
        this.coordsUser = this.calculateCheckpoints(this.pathMeasureUser);
        //this.printAllCords(this.coordsUser);
        //tev.drawCheckpoints(this.coordsUser);

        //Lösungsgraphen berechnen
        FunctionMaster fM = new FunctionMaster(tev, this.zoom);
        fM.setGrid(this.gridSettings[1],this.gridSettings[2],0.1f);
        this.initFunctionCalc(fM);

        this.critPoints = fM.critPoints();
        this.pathMeasureSolution = new PathMeasure(this.solutionPath, false);

        //Checkpoints auf dem Lösungsgraphen berechnen
        this.coordsSolution = this.calculateCheckpoints(this.pathMeasureSolution);
        this.pathLengthSolution = this.pathMeasureSolution.getLength();
        this.stepsSolution = (int) this.pathLengthSolution/stepFactor;
        this.intervalLengthSolution = pathLengthSolution/stepsSolution;
        //System.out.println("(Lösung) Pfadlänge      : "+pathLengthSolution);
        //System.out.println("(Lösung) Anzahl Schritte: "+stepsSolution);
        //System.out.println("(Lösung) Schrittlänge   : "+intervalLengthSolution);
        //this.printAllCords(this.coordsSolution);

        //Beide Graphen anhand ihrer Checkpoints miteinander vergleichen
        this.compareWithSolution(iStart, iEnd);
    }//end check()

    /**
     * Berechnet eine vorher bestimmte Anzahl an Punkten (Checkpoints), welche in gleichem Abstand auf der gemalten Kurve verteilt sind
     */
    private float[][] calculateCheckpoints(PathMeasure pM)
    {
        //hier werden die berechneten Checkpoints zurückgegeben
        float[][] checkpoints = new float[(int)(pM.getLength()/this.stepFactor)][2];

        //Informationen über übergebenen Graphen sammeln
        int stepsDone = 0;
        int steps = (int)pM.getLength()/this.stepFactor;
        float intervalLength = pM.getLength()/steps;
        float distance = 0f;
        //Berechne Koordinaten des Graphens
        while((distance < pM.getLength()) && stepsDone < steps)
        {
            pM.getPosTan(distance, pCoords ,null); //Stelle auf dem Graphen, für Koordinaten, für Winkel (tan)
            checkpoints[stepsDone][0] = pCoords[0];
            checkpoints[stepsDone][1] = pCoords[1];
            stepsDone ++;
            distance += intervalLength;
        }//end while()

        return checkpoints;
    }//end calculateCheckpoints()

    /**
     * Vergleicht den berechneten Lösungsgraphen mit dem vom Nutzer gemalten Graphen (im Angegebenen Intervall) und updated den Score
     * @param fromX         Intervallstart
     * @param toX           Intervallende
     */
    private void compareWithSolution(float fromX, float toX)
    {
        //Beide Funktionen auf angegebenes Intervall eingrenzen
        coordsUser = this.restrictToIntervall(coordsUser, fromX, toX);
        coordsSolution = this.restrictToIntervall(coordsSolution, fromX, toX);
        critPoints = this.restrictToInterval(this.critPoints, fromX, toX);

        //this.printAllCords(this.coordsUser);
        //this.printAllCords(this.coordsSolution);
        //this.printAllCrits(this.critPoints);

        float sP_diff = MAX_SCORE_FOR_DIFF/ this.coordsSolution.length;
        float sP_crit = MAX_SCORE_FOR_CRITS / this.critPoints.size();

        tev.drawCheckpoints(coordsUser);

        //Überprüfung der Länge des Graphen
        int lengthDiff = Math.abs(coordsSolution.length - coordsUser.length);
        //System.out.println("LängenDiff: "+lengthDiff);
        int scoreForLength = 0;
        if(lengthDiff == 0)
            scoreForLength += MAX_SCORE_FOR_LENGTH;
        else if(lengthDiff >= 1 && lengthDiff <= 2)
            scoreForLength += MAX_SCORE_FOR_LENGTH/1.75f;
        else if(lengthDiff >= 3 && lengthDiff <= 4)
            scoreForLength += MAX_SCORE_FOR_LENGTH/4;

        float dif = 9999;
        float difToSolution;
        int checkRadius = 2;
        float[] point = new float[2];
        int scoreForDist = 0;
        int scoreForCrits = 0;

        //Schleife durch die UserKoordinaten
        for(int iU = 0; iU<coordsUser.length; iU++)
        {
            //System.out.println("Nächster Punkt");
            difToSolution = 9999;
            for(int iS = iU-checkRadius; iS<coordsSolution.length && iS<iU+checkRadius; iS++) {
                if (iS >= 0) {
                    //Abstand zu Punkt auf dem Lösungspfad suchen, welche am nächsten liegt
                    dif = (float) Math.sqrt((float) Math.pow((coordsUser[iU][0] - coordsSolution[iS][0]), 2) + (float) Math.pow((coordsUser[iU][1] - coordsSolution[iS][1]), 2));
                    if (dif < difToSolution) {
                        difToSolution = dif;
                        point[0] = coordsSolution[iS][0];
                        point[1] = coordsSolution[iS][1];
                    }
                }
            }

            //System.out.println("Entfernung zu Lösungsgraphen: " +difToSolution);

            if(difToSolution <= 3)
                //Punkte für jeden Punkt der fast korrekt ist
                scoreForDist += sP_diff;
            else if(difToSolution <= 20)
                //Punkte für jeden Punkt im Abweichungsbereich
                scoreForDist += sP_diff/2f;
            else if(difToSolution <= 40)
                //Punkte für jeden Punkt im doppelten Abweichungsbereich
                scoreForDist += sP_diff/10;
            else
                //Punktabzug, wenn Punkt weiter als doppelter Abweichunsradius entfernt ist
                scoreForDist -= sP_diff;

            if(difToSolution <= 5 && isCrit(point))
                //Punkte für kritischen Punkt
                scoreForCrits += sP_crit;

        }//end for

        //einzelne Teilpunktzahlen auf Gesamtpunktzahl aufaddieren
        this.score += scoreForLength;
        this.score += scoreForDist;
        this.score += scoreForCrits;

        System.out.println("------------------------------------------------------------");
        System.out.println(scoreForLength + " Punkte für Länge des Graphens");
        System.out.println(scoreForDist + " Punkte für Abweichung der Checkpoints");
        System.out.println(scoreForCrits + " Punkte für Kritische Punkte");
        System.out.println("------------------");
        System.out.println(score + " Punkte Gesamt");
        System.out.println("------------------------------------------------------------");

        if(this.score < 0)
            this.score = 0;

        //Ziehe eine Bewertung aus dem berechneten Score
        this.calculateGrade();

    }//end compareWithSolution()

    /**
     * Überprüft ob der angegebene Punkt, in Form {x,y}, einer der berechneten extra
     * gewerteten Punkte der Funktion ist
     * @param p         Punkt
     * @return          true, wenn Punkt stärker gewertet werden soll. Sonst false.
     */
    private boolean isCrit(float[] p)
    {
        Set<Map.Entry<String, ArrayList<float[]>>> s = this.critPoints.entrySet();

        for(Map.Entry e: s)
        {
            ArrayList<float[]> aL = (ArrayList<float[]>) e.getValue();
            for(int i=0; i<aL.size(); i++) {
                float[] critP = aL.get(i);
                critP = this.convertToCoords(critP[0], critP[1]);

                //System.out.println("Punkt:    {"+p[0]+", "+p[1]+"}");
                //System.out.println("KritP:    {"+critP[0]+", "+critP[1]+"}");

                if((p[0] >= critP[0]-10f && p[0] <= critP[0]+10f) && (p[1] >= critP[1]-10f && p[1] <= critP[1]+10f)) {
                    //Diesen Punkt aus der Liste entfernen, damit jeder Punkt max. 1mal gewertet wird
                    ArrayList<float[]> list = this.critPoints.get(e.getKey());
                    list.remove(i);
                    critPoints.remove(e.getKey());
                    critPoints.put((String) e.getKey(), list);

                    return true;
                }
            }
        }
        return false;
    }//end isCrit()

    /**
     * Zieht aus dem erreichten Score eine Bewertung
     */
    private void calculateGrade()
    {
        System.out.println("Score: " + this.score);
        int grade = 0;

        if(this.score >= 800)
            grade = 1;
        else if(this.score >= 500)
            grade = 2;
        else if(this.score >= 350)
            grade = 3;
        else if(this.score < 350)
            grade = 4;

        //wenn Fehlversuch
        if(grade == 4)
        {
            //Gemalten Graphen zurücksetzen
            tev.reset();
            //Malen wieder ermöglichen
            tev.lock(false);
        }
        //wenn Korrekt
        else
            //Lösung anzeigen
            tev.showSolutionPath(true);

        tev.grading(this.score, grade);

    }//end score()

    /**
     * Grenzt ein übergebenes 2-dim. Array auf das angegebene Intervall ein
     * @param coords    Array, welches eingegrenz werden soll
     * @param fromX     Intervallstart
     * @param toX       Intervallende
     * @return          Array, welches mur Koordinatenpaare enthält, welche innerhalb des Intervalls liegen
     */
    private float[][] restrictToIntervall(float[][]coords, float fromX, float toX)
    {
        //Intervall umgerechnet auf Screenpixel
        float intervalStart = this.intervallToCoords(fromX);
        float intervalEnd = this.intervallToCoords(toX);

        int offset = -1;
        int length = 0;

        for(int i=0; i<coords.length; i++)
        {
            if((coords[i][0] >= intervalStart && coords[i][0] <= intervalEnd))
            {
                if(offset == -1)
                    offset = i; //wenn erstes Koordinatenpaar in Intervall, als Offset setzen
                length++; //"Anzahl" der Paare im Intevall erhöhen
            }
        }

        if(offset != -1 && length!=0)
        {
            float[][] coordsInIntervall = new float[length][2];
            System.arraycopy(coords, offset, coordsInIntervall, 0, length);
            return coordsInIntervall;
        }
        return coords;
    }//end restrictToIntervall()

    /**
     * Grenzt eine übergebene HashMap mit kritischen Punkten auf das angegebene Intervall ein
     * @param crits     HaspMap, welches eingegrenzt werden soll
     * @param fromX     Intervallstart
     * @param toX       Intervallende
     * @return          HashMap, welches nur Koordinatenpaare enthält, welche innerhalb des Intervalls liegen
     */
    private HashMap<String, ArrayList<float[]>> restrictToInterval(HashMap<String, ArrayList<float[]>> crits, float fromX, float toX)
    {
        String[] keys = new String[] {"Nullpunkt", "y-Achsenabschnitt", "Hochpunkt", "Tiefpunkt"};

        for(int i=0; i<keys.length; i++) {
            ArrayList<float[]> points = crits.get(keys[i]);
            if(points != null) {
                crits.remove(keys[i]);
                crits.put(keys[i], this.restrictPoints(points, fromX, toX));
            }
        }
        return crits;
    }//end restrictToInterval

    /**
     * Schränkt eine übergebene ArrayList auf das angegebene Intervall ein
     * @param aL            ArrayList
     * @param fromX         IntervallStart
     * @param toX           IntervallEnde
     * @return              ArrayList (eingeschränkt)
     */
    private ArrayList<float[]> restrictPoints(ArrayList<float[]> aL, float fromX, float toX)
    {
        //Intervall umgerechnet auf Screenpixel
        float intervalStart = this.intervallToCoords(fromX-0.15f);
        float intervalEnd = this.intervallToCoords(toX+0.15f);

        ArrayList<float[]> restrictedAL = new ArrayList<>();

        for (float[] pair : aL) {
            float[] pairConverted = this.convertToCoords(pair[0], pair[1]);
            if (pairConverted[0] >= intervalStart && pairConverted[0] <= intervalEnd)
                restrictedAL.add(pair);
        }

        return restrictedAL;
    }//end restrictPoints()

    /**
     * Rechnet eine Intervalleingabe in Screenpixelangaben um
     * @param intervallBorder       Intervall Grenze
     * @return                      Screenkoordinaten dieser Grenze
     */
    private float intervallToCoords(float intervallBorder)
    {
        float xZero = tev.getWidth()/2;
        //System.out.println("Koordinatenurpsrung bei x = "+xZero);
        return ((intervallBorder*this.zoom)+xZero);
    }//end intervallToCoords()

    /**
     * Gibt alle Koordinaten des übergebenen Arrays auf der Konsole aus
     * @param coords        2-dim. Array, welches Koordinatenpaare enthält
     */
    private void printAllCords(float[][] coords)
    {
        for(int i=0; i<coords.length; i++)
            System.out.println("Checkpoint " + i + ": x " + coords[i][0] + ",  y " + coords[i][1]);
    }//end printAllCords()

    private void printAllCrits(HashMap<String, ArrayList<float[]>> crits)
    {
        String[] keys = new String[] {"Nullpunkt", "y-Achsenabschnitt", "Hochpunkt", "Tiefpunkt"};

        for(int i=0; i<keys.length; i++) {
            System.out.println("-------------------------------------");
            System.out.println( keys[i] + ":");
            ArrayList<float[]> points = crits.get(keys[i]);
            if(points != null) {
                for (float[] p : points) {
                    System.out.println(p[0] + ", " + p[1]);
                }
            }
        }
    }//end printAllCrits()

    /**
     * Wandelt Funktionswerte in Bildschirmkoordinaten um
     * @param x     x-Koordinate
     * @param y     y-Koordinate
     * @return      float-Array mit {x,y} umgewandelt in Bildschirmkoordinaten
     */
    private float[] convertToCoords(float x, float y)
    {
        float xZero = tev.getWidth()/2;
        float yZero = tev.getHeight()/2;

        return new float[] {(x*this.zoom+xZero), tev.getHeight()-(y*this.zoom+yZero)};
    }//end convertToCoords()

    /**
     * Sorgt dafür, dass der FunctionMaster den richtigen Funktionstypen mit den richtigen Funktionewerten berechnet
     * @param fM        FunctionMaster, welcher für Berechnung des Lösungsgraphen verantwortlich ist
     */
    private void initFunctionCalc(FunctionMaster fM)
    {
        ArrayList<Float> functionValues = this.decodeFunct();

        float t = functionValues.get(0);
        int type = (int) t;
        switch(type) //erste Stelle gibt Funktionstypen an
        {
            case 1: // 1 = Sinusfunktion
                this.solutionPath = fM.sin(functionValues.get(1), functionValues.get(2), functionValues.get(3), functionValues.get(4));
                break;
            case 2: // 2 = Cosinusfunktion
                this.solutionPath = fM.cos(functionValues.get(1), functionValues.get(2), functionValues.get(3), functionValues.get(4));
                break;
            case 3: // 3 = Tangensfunktion
                this.solutionPath = fM.tan(functionValues.get(1), functionValues.get(2), functionValues.get(3), functionValues.get(4));
                break;
            case 4: // 4 = Polynomfunktion
                this.solutionPath = fM.poly(functionValues.get(1), functionValues.get(2), functionValues.get(3), functionValues.get(4));
                break;
            default:
                System.out.println("FUNKTIONSTYP NICHT ERKANNT!");

        }
    }//end initFunctionCalc()

    /**
     * Liest die Werte des aud der Datenbank gelesenen Funktionsstring aus und speichert diese ab.
     * @return      ArrayList mit den Funktionswerten als float
     */
    private ArrayList<Float> decodeFunct()
    {
        ArrayList fV = new ArrayList();
        char[] function = this.funct.toCharArray();

        float value = 0;
        float decPl = 0;
        boolean negative = false;

        for(int i=0; i< function.length; i++)
        {
            if(function[i] != ';') //Ausschluss von Seperationszeichen
            {
                if(function[i] == '-')
                    negative = true;
                else if(function[i] == ',' || function[i] == '.') {
                    decPl = Character.getNumericValue(function[i+1]);
                    i++;
                }
                else
                    value = Character.getNumericValue(function[i]);
            }
            else // ';' trennt Werte voneinander. Schreiben des aktuell gelesenen Wertes in Liste
            {
                if(!negative)
                    fV.add(value + (decPl/10));
                else
                    fV.add((value + (decPl/10))*(-1f)); //negieren wenn negativ
                value = decPl = 0;
                negative = false;
            }
        }

        return fV;
    }// decodeFunct()

}//end class
