package de.benediktstuhrmann.drawingtests;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends Activity {

    Button btn_start;
    Button btn_settings;
    UserDataSource uds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        final Context context = this;
        uds = new UserDataSource(context);

        try {
            uds.open();
            uds.init();
            uds.close();
            System.out.println("Initialisierung erfolgreich");
        }catch(Exception e) {
            System.out.println("Einträge bestehen bereits");
        }

        btn_start = (Button) findViewById(R.id.btn_start);
        btn_settings = (Button) findViewById(R.id.btn_settings);

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent swapToDraw = new Intent(context, LevelSelectActivity.class);
                startActivity(swapToDraw);
            }
        });


        btn_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder a_builder = new AlertDialog.Builder(MainMenuActivity.this);
                a_builder.setMessage("Bist du dir sicher, dass du all deine Spielstände zurücksetzten willst? \nAlle deine gespeicherten Fortschritte gehen dabei verloren!")
                        .setCancelable(false)
                        .setPositiveButton("Ja",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    uds.open();
                                    uds.deleteAndRecreate();
                                    uds.init();
                                    uds.show();
                                    uds.close();
                                    System.out.println("Löschen und Neuerstellen erfolgreich");

                                }catch(Exception e)
                                {
                                    System.out.println("DaR Error");
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton("Abbrechen",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }) ;
                AlertDialog alert = a_builder.create();
                alert.setTitle("Wirklich zurücksetzten?");
                alert.show();
            }
        });

    }//end onCreate()

}//end class
